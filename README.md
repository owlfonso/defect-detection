# Defect Detection 
A CNN implementation of a code case to detect images with or without defect.

This project is developed using Google Colab for GPU

1- Clone repo to Google Drive

    %cd /content/drive/
    !git clone https://gitlab.com/owlfonso/defect-detection/
    
2- Upload dataset Class6 to defect-detection folder in Drive
 